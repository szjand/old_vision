program pdqStoreData;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  pdqStoreDataDM in 'pdqStoreDataDM.pas' {DM: TDataModule};

resourcestring
  executable = 'pdqStoreData';

begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
//      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
//        QuotedStr(executable) + ', ' +
//        '''' + 'none' + '''' + ', ' +
//        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
// *** active procs ***//
      DM.xfmPdqStoreData;
// *** active procs ***//
//      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
//        QuotedStr(executable) + ', ' +
//        '''' + 'none' + '''' + ', ' +
//        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//        'null' + ')');
    except
      on E: Exception do
      begin
//        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
//          QuotedStr(executable) + ', ' +
//          '''' + 'none' + '''' + ', ' +
//          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        DM.SendMail(executable + ' ' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
