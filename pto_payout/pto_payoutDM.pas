unit pto_payoutDM;

interface

uses
  SysUtils, Classes,
  IdBaseComponent,
  IdComponent,
  IdTCPConnection,
  IdTCPClient,
  IdExplicitTLSClientServerBase,
  IdFTP,
  IdAllFTPListParsers, adsdata, adsfunc, adstable, adscnnct, DB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SendMail(Recipient: string; Subject: string; Body: string='');
    procedure populatePtoPayoutEligible;
    procedure employeeEmails; // employee
    procedure authEmails;  // pto authorizer
    procedure auth_authEmails; // pto authorizer authorizer
    procedure adminEmails; // cahalan, foster, andrews
  end;

var
  DM: TDM;
  AdsCon: TADSConnection;
  EmployeeQuery: TAdsQuery;
  AuthorizerQuery: TAdsQuery;
  eligibleEmployees: integer;



  updateRequestsPassed: boolean = false;
  updateUsersPassed: boolean = false;
  updatePtoUsedPassed: boolean = false;
  eMessage: string = '';
  msg: string = '';
  employeeEmail: string = '';
  authEmail: string = '';
  authAuthEmail: string = '';
implementation

{$R *.dfm}

{ TDM }


constructor TDM.Create(AOwner: TComponent);
begin
  AdsCon := TADSConnection.Create(nil);
  EmployeeQuery := TADSQuery.Create(nil);
  EmployeeQuery.AdsConnection := AdsCon;
  AuthorizerQuery := TADSQuery.Create(nil);
  AuthorizerQuery.AdsConnection := AdsCon;
//  AdsCon.ConnectPath := '\\67.135.158.12:6363\DailyCut\scoTest\wednesday\copy\sco.add';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\advantage\scotest\sco.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
end;

destructor TDM.Destroy;
begin
  EmployeeQuery.Close;
  AuthorizerQuery.Close;
  FreeAndNil(EmployeeQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  inherited;
end;

procedure TDM.populatePtoPayoutEligible;
begin
  EmployeeQuery.Close;
  EmployeeQuery.SQL.Clear;
  EmployeeQuery.SQL.Text := 'EXECUTE PROCEDURE pto_payout_data();';
  EmployeeQuery.Open;
  eligibleEmployees := EmployeeQuery.FieldByName('the_count').AsInteger;
  EmployeeQuery.Close;
end;


procedure TDM.employeeEmails;
var
  Recipient: string;
  Subject: string;
  ExpDate: string;
  Body: string;
  Hours: string;
  Authorizer: string;
begin
  EmployeeQuery.Close;
  EmployeeQuery.SQL.Clear;
  EmployeeQuery.SQL.Text := 'execute procedure pto_payout_employee_email();';
  EmployeeQuery.Open;
  while not EmployeeQuery.Eof do
  begin
{ DONE : Replace jandrews with employee_email }
    Recipient := EmployeeQuery.FieldByName('employee_email').AsString;
    Subject := 'Unused PTO';
    ExpDate := EmployeeQuery.FieldByName('current_pto_period_thru').AsString;
    Hours := EmployeeQuery.FieldByName('pto_unused').AsString;
    Authorizer := EmployeeQuery.FieldByName('authorizer').AsString;
    Body := EmployeeQuery.FieldByName('employee_email').AsString + sLineBreak +
      'Records show that your current PTO period expires on ' + ExpDate +
      ' and that you have ' + Hours + ' hours of unused PTO.' + sLineBreak +
      'Please see ' + Authorizer + ' to determine if you are eligible ' +
      'for payout of the unused PTO.';
    SendMail(Recipient,Subject,Body);
    EmployeeQuery.Next;
  end;
end;


procedure TDM.authEmails;
var
  Recipient: string;
  Subject: string;
  ExpDate: string;
  Body: string;
  Hours: string;
  Employee: string;
//  AuthorizerEmail: string;
begin
  AuthorizerQuery.Close;
  AuthorizerQuery.SQL.Clear;
  AuthorizerQuery.SQL.Text := 'SELECT DISTINCT authorizer_email FROM pto_payout_eligible;';
  AuthorizerQuery.Open;
  while not AuthorizerQuery.Eof do
  begin
{ DONE : Replace jandrews with authorizer email }
    Recipient := AuthorizerQuery.FieldByName('authorizer_email').AsString;
    Subject := 'PTO Payout - auth ' + AuthorizerQuery.FieldByName('authorizer_email').AsString;
    Body := 'The following employee(s) current Pto period expires this week and records '  + sLineBreak +
      ' show that they have unused PTO and may be eligible for a PTO payout. '  + sLineBreak +
      ' Please consult with them and, if appropriate, complete the "PTO Payout Request" form in Compli.';
    EmployeeQuery.Close;
    EmployeeQuery.SQL.Clear;
    EmployeeQuery.SQL.Text := 'execute procedure pto_payout_authorizer_email(' +
      QuotedStr(AuthorizerQuery.FieldByName('authorizer_email').AsString) + ')';
    EmployeeQuery.Open;
    while not EmployeeQuery.Eof do
    begin
      Employee := EmployeeQuery.FieldByName('employee_name').AsString;
      ExpDate := EmployeeQuery.FieldByName('current_pto_period_thru').AsString;
      Hours := EmployeeQuery.FieldByName('pto_unused').AsString;
      Body := Body + sLineBreak + '-----------------------------------         ';
      Body := Body + sLineBreak + Employee +
        ':  PTO expires on ' + ExpDate + ' with a balance of ' + Hours + ' unused hours.';
      EmployeeQuery.Next;
    end;
    SendMail(Recipient,Subject,Body);
    AuthorizerQuery.Next;
  end;
end;

procedure TDM.auth_authEmails;
var
  Recipient: string;
  Subject: string;
  ExpDate: string;
  Body: string;
  Hours: string;
  Employee: string;
  Authorizer: string;
begin
  AuthorizerQuery.Close;
  AuthorizerQuery.SQL.Clear;
  AuthorizerQuery.SQL.Text := 'SELECT DISTINCT authorizer_authorizer_email FROM pto_payout_eligible;';
  AuthorizerQuery.Open;
  while not AuthorizerQuery.Eof do
  begin
{ DONE : Replace jandrews with authorizer authorizer email }
    Recipient := AuthorizerQuery.FieldByName('authorizer_authorizer_email').AsSTring;
    Subject := 'PTO Payout - auth_auth ' + AuthorizerQuery.FieldByName('authorizer_authorizer_email').AsString;
    Body := 'The following employee(s) current Pto period expires this week and records '  + sLineBreak +
      ' show that they have unused PTO and may be eligible for a PTO payout.';
    EmployeeQuery.Close;
    EmployeeQuery.SQL.Clear;
    EmployeeQuery.SQL.Text := 'execute procedure pto_payout_authorizer_authorizer_email(' +
      QuotedStr(AuthorizerQuery.FieldByName('authorizer_authorizer_email').AsString) + ')';
    EmployeeQuery.Open;
    while not EmployeeQuery.Eof do
    begin
      Employee := EmployeeQuery.FieldByName('employee_name').AsString;
      ExpDate := EmployeeQuery.FieldByName('current_pto_period_thru').AsString;
      Hours := EmployeeQuery.FieldByName('pto_unused').AsString;
      Authorizer := EmployeeQuery.FieldByName('authorizer').AsString;
      Body := Body + sLineBreak + '-----------------------------------         ';
      Body := Body + sLineBreak + Employee +
        ':  PTO expires on ' + ExpDate + ' with a balance of ' + Hours + ' unused hours. ' +
        'PTO authorized by ' +  Authorizer + '.';
      EmployeeQuery.Next;
    end;
    SendMail(Recipient,Subject,Body);
    AuthorizerQuery.Next;
  end;
end;


procedure TDM.adminEmails;
var
  Recipient: string;
  Subject: string;
  ExpDate: string;
  Body: string;
  Hours: string;
  Authorizer: string;
  Employee: string;
begin
  EmployeeQuery.Close;
  EmployeeQuery.SQL.Clear;
  EmployeeQuery.SQL.Text := 'execute procedure pto_payout_employee_email();';
  EmployeeQuery.Open;
  Body := 'The following employee(s) current PTO period expires this week and records '  + sLineBreak +
    ' show that they have unused PTO and may be eligible for a PTO payout.';
  while not EmployeeQuery.Eof do
  begin
{ DONE : Replace jandrews with admin list - cahalan, foster }
    Employee := EmployeeQuery.FieldByName('employee_name').AsString;
    Recipient := 'jandrews@cartiva.com,gsorum@cartiva.com,tbroyles@rydellcars.com,bfoster@rydellcars.com,bcahalan@rydellcars.com';
    Subject := 'PTO Payout - Admin';
    ExpDate := EmployeeQuery.FieldByName('current_pto_period_thru').AsString;
    Hours := EmployeeQuery.FieldByName('pto_unused').AsString;
    Authorizer := EmployeeQuery.FieldByName('authorizer').AsString;
    Body := Body + sLineBreak + '-----------------------------------         ';
    Body := Body + sLineBreak + Employee +
      ':  PTO expires on ' + ExpDate + ' with a balance of ' + Hours + ' unused hours. ' +
      'PTO authorized by ' +  Authorizer + '.';
    EmployeeQuery.Next;
  end;
  SendMail(Recipient,Subject,Body);
end;



procedure TDM.SendMail(Recipient, Subject, Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
//  Msg.Recipients.Add.Text := Recipient;
  Msg.Recipients.EMailAddresses := Recipient;
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
//        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
//            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
//            '''' + 'send mail failed' + '''' + ', ' +
//            '''' + 'no sql - line 427' + '''' + ', ' +
//            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;



end.
