unit ocrDM;

interface

uses
  SysUtils, Classes,
  IdAllFTPListParsers, adsdata, adsfunc, adstable, adscnnct, DB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SendMail(Subject: string; Body: string='');
    procedure glDetailsTable;
    procedure updateVehicleCostTable;
    procedure createGrossLogTable;
  end;

var
  DM: TDM;
  AdsCon: TADSConnection;
  AdsQuery: TAdsQuery;
  glDetailsTablePassed: boolean = false;
  updateVehicleCostTablePassed: boolean = false;
  createGrossLogTablePassed: boolean = false;
  eMessage: string = '';
implementation

{$R *.dfm}

{ TDM }

constructor TDM.Create(AOwner: TComponent);
begin
  AdsCon := TADSConnection.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdsQuery.AdsConnection := AdsCon;
//  adsCon.ConnectPath := '\\67.135.158.12:6363\DailyCut\dpsVSeries\Sunday\copy\dpsVSeries.add';
  AdsCon.ConnectPath := '\\96.3.202.12:6363\advantage\dpsvseries\dpsvseries.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
end;

procedure TDM.glDetailsTable;
begin
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'EXECUTE PROCEDURE udpateGlDetailsTable()';
    AdsQuery.ExecSQL;
    glDetailsTablePassed := true;
  except
    on E: Exception do
    begin
      if length(eMessage) > 6 then
        eMessage := eMessage  + #13#10 +  'old_crayon_report.glDetailsTable failed: ' + E.Message
      else
        eMessage := eMessage + ' old_crayon_report.glDetailsTable failed: ' + E.Message;
      exit;
    end;
  end;
end;

procedure TDM.updateVehicleCostTable;
begin
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'execute procedure updateVehicleCostTable()';
    AdsQuery.ExecSQL;
    updateVehicleCostTablePassed := true;
  except
    on E: Exception do
    begin
      if length(eMessage) > 6 then
        eMessage := eMessage  + #13#10 +  'old_crayon_report.updateVehicleCostTable failed: ' + E.Message
      else
        eMessage := eMessage + ' old_crayon_report.updateVehicleCostTable failed: ' + E.Message;
      exit;
    end;
  end;
end;

procedure TDM.createGrossLogTable;
begin
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'execute procedure createGrossLogTable()';
    AdsQuery.ExecSQL;
    createGrossLogTablePassed := true;
  except
    on E: Exception do
    begin
      if length(eMessage) > 6 then
        eMessage := eMessage  + #13#10 +  'old_crayon_report.createGrossLogTable failed: ' + E.Message
      else
        eMessage := eMessage + ' old_crayon_report.createGrossLogTable failed: ' + E.Message;
      exit;
    end;
  end;
end;


destructor TDM.Destroy;
begin
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  inherited;
end;

procedure TDM.SendMail(Subject, Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
//        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
//            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
//            '''' + 'send mail failed' + '''' + ', ' +
//            '''' + 'no sql - line 427' + '''' + ', ' +
//            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


end.
