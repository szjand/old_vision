unit scoTodayDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure swMetricDataToday;
    procedure GLPDTIM;
    procedure GLPTRNS;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'scoToday';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdsCon.ConnectPath := '\\172.17.196.12:6363\Advantage\scotest\sco.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;


procedure TDM.swMetricDataToday;
var
  proc: string;
begin
  proc := 'swMetricDataToday';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure swMetricDataToday()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('swMetricDataToday.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
  end;
end;


procedure TDM.GLPDTIM;
var
  proc: string;
begin
  proc := 'GLPDTIM';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpGLPDTIM'')');
      CloseQuery(adsQuery);
      PrepareQuery(AdsQuery,'insert into tmpGLPDTIM ' +
          '(gqco#, gqtrn#, gquser, gqdate, gqtime, gqfunc) values ' +
          '(:GQCO, :GQTRN, :GQUSER, :GQDATE, :GQTIME, :GQFUNC)');
      OpenQuery(AdoQuery, 'select gqco#,gqtrn#,gquser,gqdate,gqtime,gqfunc from ' +
          'rydedata.glpdtim where ' +
          'gqdate = year(curdate())*10000 + month(curdate())*100 + day(curdate()) ' +
          'and gqtime <> 0' + ' and gqtrn# <> 0');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('GQCO').AsString := AdoQuery.FieldByName('GQCO#').AsString;
        AdsQuery.ParamByName('GQTRN').AsInteger := AdoQuery.FieldByName('GQTRN#').AsInteger;
        AdsQuery.ParamByName('GQUSER').AsString := AdoQuery.FieldByName('GQUSER').AsString;
        AdsQuery.ParamByName('GQDATE').AsInteger := AdoQuery.FieldByName('GQDATE').AsInteger;
        AdsQuery.ParamByName('GQTIME').AsInteger := AdoQuery.FieldByName('GQTIME').AsInteger;
        AdsQuery.ParamByName('GQFUNC').AsString := AdoQuery.FieldByName('GQFUNC').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE tmpGLPTIMaddTS()'); // populates the gqTS field
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpGLPDTIM'')');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('glpdtim.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.GLPTRNS;
// 7/29/19
// arkona no longer recognizes system field names, so changed them to the
// "new" names
// including the typo for gtodoc: ORIGINAL_DOCUCMENT
// changed error message to specify tmpGLPTRNS
var
  proc: string;
begin
  proc := 'GLPTRNS';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpGLPTRNS'')');
      CloseQuery(adsQuery);
      PrepareQuery(AdsQuery, 'insert into tmpGLPTRNS values' +
          '(:GTCO, :GTTRN, :GTSEQ, :GTDTYP, :GTTYPE, :GTPOST, :GTRSTS,' +
          ':GTADJUST, :GTPSEL, :GTJRNL, :GTDATE, :GTRDATE, :GTSDATE,' +
          ':GTACCT, :GTCTL, :GTDOC, :GTRDOC, :GTRDTYP, :GTODOC,' +
          ':GTREF, :GTVND, :GTDESC, :GTTAMT, :GTCOST, :GTCTLO, :GTOCO)');
      OpenQuery(AdoQuery, 'select * from rydedata.glptrns where gtpost = ''Y''' +
          ' and gttrn# in (select gqtrn# from rydedata.glpdtim where gqtrn# <> 0' +
          ' and gqtime <> 0 and gqdate = year(curdate())*10000 + month(curdate())*100 + day(curdate()))');
      while not AdoQuery.eof do
      begin
        AdsQuery.ParamByName('GTCO').AsString := AdoQuery.FieldByName('COMPANY_NUMBER').AsString;
        AdsQuery.ParamByName('GTTRN').AsInteger := AdoQuery.FieldByName('TRANSACTION_NUMBER').AsInteger;
        AdsQuery.ParamByName('GTSEQ').AsInteger := AdoQuery.FieldByName('SEQUENCE_NUMBER').AsInteger;
        AdsQuery.ParamByName('GTDTYP').AsString := AdoQuery.FieldByName('DOCUMENT_tYPE').AsString;
        AdsQuery.ParamByName('GTTYPE').AsString := AdoQuery.FieldByName('RECORD_TYPE').AsString;
        AdsQuery.ParamByName('GTPOST').AsString := AdoQuery.FieldByName('POST_STATUS').AsString;
        AdsQuery.ParamByName('GTRSTS').AsString := AdoQuery.FieldByName('RECONCILE_STATUS').AsString;
        AdsQuery.ParamByName('GTADJUST').AsString := AdoQuery.FieldByName('ADJUSTMENT_TRANSACTION').AsString;
        AdsQuery.ParamByName('GTPSEL').AsString := AdoQuery.FieldByName('SELECT_TO_PAY').AsString;
        AdsQuery.ParamByName('GTJRNL').AsString := AdoQuery.FieldByName('JOURNAL').AsString;
        AdsQuery.ParamByName('GTDATE').AsDateTime := AdoQuery.FieldByName('TRANSACTION_DATE').AsDateTime;
        AdsQuery.ParamByName('GTRDATE').AsDateTime := AdoQuery.FieldByName('RECONCILE_DATE').AsDateTime;
        AdsQuery.ParamByName('GTSDATE').AsDateTime := AdoQuery.FieldByName('STATEMENT_dATE').AsDateTime;
        AdsQuery.ParamByName('GTACCT').AsString := AdoQuery.FieldByName('ACCOUNT_NUMBER').AsString;
        AdsQuery.ParamByName('GTCTL').AsString := AdoQuery.FieldByName('CONTROL_NUMBER').AsString;
        AdsQuery.ParamByName('GTDOC').AsString := AdoQuery.FieldByName('DOCUMENT_NUMBER').AsString;
        AdsQuery.ParamByName('GTRDOC').AsString := AdoQuery.FieldByName('RECONCILE_DOCUMENT').AsString;
        AdsQuery.ParamByName('GTRDTYP').AsString := AdoQuery.FieldByName('RECONCILE_DOCUMENT_TYPE').AsString;
        AdsQuery.ParamByName('GTODOC').AsString := AdoQuery.FieldByName('ORIGINAL_DOCUCMENT').AsString;
        AdsQuery.ParamByName('GTREF').AsString := AdoQuery.FieldByName('REFERENCE_NUMBER').AsString;
        AdsQuery.ParamByName('GTVND').AsString := AdoQuery.FieldByName('VENDOR_NUMBER').AsString;
        AdsQuery.ParamByName('GTDESC').AsString := AdoQuery.FieldByName('DESCRIPTION').AsString;
        AdsQuery.ParamByName('GTTAMT').AsCurrency := AdoQuery.FieldByName('TRANSACTION_AMOUNT').AsCurrency;
        AdsQuery.ParamByName('GTCOST').AsCurrency := AdoQuery.FieldByName('COST').AsCurrency;
        AdsQuery.ParamByName('GTCTLO').AsString := AdoQuery.FieldByName('CONTROL_OVERRIDE').AsString;
        AdsQuery.ParamByName('GTOCO').AsString := AdoQuery.FieldByName('ORIGINATING_COMPANY').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpGLPTRNS'')');
//      ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE StgArkGLPTRNS()');
//      curLoadPrevLoad('GLPTRNS');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('tmpGLPTRNS  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


//************************Utilities*******************************************//

function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;


end.


