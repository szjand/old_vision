program scoYesterday;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  scoYesterdayDM in 'scoYesterdayDM.pas' {DM: TDataModule};

resourcestring
  executable = 'scoYesterday';

begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
// *** active procs ***//
      DM.swMetricDataYesterday;
// *** active procs ***//
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          '''' + 'none' + '''' + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        DM.SendMail('sco.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
