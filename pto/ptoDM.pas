unit ptoDM;

interface

uses
  SysUtils, Classes,
  IdBaseComponent,
  IdComponent,
  IdTCPConnection,
  IdTCPClient,
  IdExplicitTLSClientServerBase,
  IdFTP,
  IdAllFTPListParsers, adsdata, adsfunc, adstable, adscnnct, DB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SendMail(Subject: string; Body: string='');
    procedure ftpCompliFiles;
    procedure loadUsers;
    procedure loadTimeOff;
    procedure updateRequests;
    procedure updateUsers;
    procedure updatePtoUsed;
  end;

var
  DM: TDM;
  AdsCon: TADSConnection;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;
  ftpPassed: boolean = false;
  loadUsersPassed: boolean = false;
  loadTimeOffPassed: boolean = false;
  updateRequestsPassed: boolean = false;
  updateUsersPassed: boolean = false;
  updatePtoUsedPassed: boolean = false;
  eMessage: string = '';
implementation

{$R *.dfm}

{ TDM }

constructor TDM.Create(AOwner: TComponent);
begin
  AdsCon := TADSConnection.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdsQuery.AdsConnection := AdsCon;
//  AdsCon.ConnectPath := '\\67.135.158.12:6363\DailyCut\scoTest\wednesday\copy\sco.add';
  AdsCon.ConnectPath := '\\10.130.196.252:6363\advantage\scotest\sco.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
end;

destructor TDM.Destroy;
begin
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  inherited;
end;


procedure TDM.ftpCompliFiles;
var
  ftp: TidFTP;
  i: integer;
  localFolder: string;
  x: tDateTime;
begin
// Download Files
(**)
  ftp := TIdFTP.Create();
//  localFolder := '.\files\';
// needed this on scraper server, from a scheduled task, the files were being
//   looked for in C:\Windows\system32\files\ExportUsers.csv
//  do not forget on server scheduled tasks: set the start in, once that was
//  done, the relative path to localFolder worked just fin
  localFolder := '.\files\';
  try
    try
//      ftp.Host := '10.130.197.226';
      ftp.Host := '10.130.196.138';
      ftp.UserName := 'rydell';
      ftp.Password := '1rydell$';
      ftp.Connect;
      ftp.ChangeDir('/Downloads/compli_dls/');
//      ftp.ChangeDir('/Downloads/');
      x := ftp.FileDate('ExportUsers.csv');
      if DateToStr(ftp.FileDate('ExportUsers.csv')) <> DateToStr(now) then
        raise Exception.Create('File date is not current');
      ftp.List(NIL,'*.csv');
      for i := 0 to ftp.DirectoryListing.Count - 1 do
        begin
          FTP.Get(FTP.DirectoryListing[I].FileName, localFolder + FTP.DirectoryListing[I].FileName, True, False);
        end;
      ftp.Abort;
      ftp.Quit;
      ftpPassed := true;
    except
      on E: Exception do
      begin
        eMessage := 'TDM.ftpCompliFiles failed: ' + E.Message;
        exit;
      end;
    end;
  finally
    ftp.Free;
  end;
end;

procedure TDM.loadTimeOff;
var
  F: TextFile;
  FileName: string;
  s1: TStringList;
  i: integer;
  text: string;
begin

  s1 := TSTringList.Create;
  FileName := '.\files\time_allocation_detail.csv';
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'delete from pto_tmp_compli_timeoff';
    AdsQuery.ExecSQL;
    AssignFile(F, FileName);
    Reset(F);
    ReadLn(F);
    ReadLn(F);
    ReadLn(F);
    with AdsQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('insert into pto_tmp_compli_timeoff (');
      SQL.Add('lastName,firstName,userid,hireDate,requestStatus,scheduledDate, ' +
        'hours,allocationType,currentlyWith,startedOn,lastSent,filedOn, ' +
        'fromTitle)');
      SQL.Add('values (');
      SQL.Add(':AlastName,:AfirstName,:Auserid,:AhireDate,:ArequestStatus,:AscheduledDate, ' +
        ':Ahours,:AallocationType,:AcurrentlyWith,:AstartedOn,:AlastSent,:AfiledOn, ' +
        ':AfromTitle)');
    end;
    try
      while not EOF(F) do
      begin
        ReadLn(F, text);
        s1.Add(text);
        s1.DelimitedText := s1.Text;
        AdsQuery.Params.ParamByName('AlastName').AsString := s1[0];
        AdsQuery.Params.ParamByName('AfirstName').AsString := s1[1];
        AdsQuery.Params.ParamByName('Auserid').AsString := s1[2];
        AdsQuery.Params.ParamByName('AhireDate').AsString := s1[3];
        AdsQuery.Params.ParamByName('ArequestStatus').AsString := s1[4];
        AdsQuery.Params.ParamByName('AscheduledDate').AsString := s1[5];
        AdsQuery.Params.ParamByName('Ahours').AsString := s1[6];
        AdsQuery.Params.ParamByName('AallocationType').AsString := s1[7];
        AdsQuery.Params.ParamByName('AcurrentlyWith').AsString := s1[8];
        AdsQuery.Params.ParamByName('AstartedOn').AsString := s1[9];
        AdsQuery.Params.ParamByName('AlastSent').AsString := s1[10];
        AdsQuery.Params.ParamByName('AfiledOn').AsString := s1[11];
        AdsQuery.Params.ParamByName('AfromTitle').AsString := s1[12];
        AdsQuery.ExecSQL;
        for i := 0 to AdsQuery.ParamCount -1 do
          AdsQuery.Params.Items[i].AsString := '';
        s1.Clear;
      end;
      loadTimeoffPassed := true;
    except
      on E: Exception do
      begin
        if length(eMessage) > 6 then
          eMessage := eMessage  + #13#10 +  'TDM.loadTimeOff failed: ' + E.Message
        else
          eMessage := eMessage + ' TDM.loadTimeOff failed: ' + E.Message;
        exit;
      end;
    end;
  finally
    s1.Free;
    CloseFile(F);
  end;
end;

procedure TDM.loadUsers;
var
  F: TextFile;
  FileName: string;
  s1: TStringList;
  i: integer;
  text: string;
begin
  s1 := TSTringList.Create;
  FileName := '.\files\ExportUsers.csv';
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'delete from pto_tmp_compli_users';
    AdsQuery.ExecSQL;
    AssignFile(F, FileName);
    Reset(F);
    ReadLn(F);
    with AdsQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('insert into pto_tmp_compli_users (');
      SQL.Add('userid,FirstName,LastName,Email,SupervisorID,SupervisorName, ' +
        'LocationID,LocationName,DateofHire,Title,Status,PolicyAssigned, ' +
        'PolicySigned,TrainingAssigned,TrainingCompleted,FormsAssigned, ' +
        'FormsCompleted,FormsInNotification,Exclusionary,DriversLicenseState, ' +
        'DriversLicenseID) ');
      SQL.Add('values (');
      SQL.Add(':Auserid,:AFirstName,:ALastName,:AEmail,:ASupervisorID,:ASupervisorName, ' +
        ':ALocationID,:ALocationName,:ADateofHire,:ATitle,:AStatus,:APolicyAssigned, ' +
        ':APolicySigned,:ATrainingAssigned,:ATrainingCompleted,:AFormsAssigned, ' +
        ':AFormsCompleted,:AFormsInNotification,:AExclusionary,:ADriversLicenseState, ' +
        ':ADriversLicenseID)');
    end;
    try
      while not EOF(F) do
      begin
        ReadLn(F, text);
        s1.Add(text);
        s1.DelimitedText := s1.Text;
    // the csv file includes an extra line at the end that consists of
    // only a CRLF which caused this to error out with List Index out of Bounds
        while length(s1.Text) > 3 do
        begin;
          AdsQuery.Params.ParamByName('Auserid').AsString := s1[0];
          AdsQuery.Params.ParamByName('AFirstName').AsString := s1[1];
          AdsQuery.Params.ParamByName('ALastName').AsString := s1[2];
          AdsQuery.Params.ParamByName('AEmail').AsString := s1[3];
          AdsQuery.Params.ParamByName('ASupervisorID').AsString := s1[4];
          AdsQuery.Params.ParamByName('ASupervisorName').AsString := s1[5];
          AdsQuery.Params.ParamByName('ALocationID').AsString := s1[6];
          AdsQuery.Params.ParamByName('ALocationName').AsString := s1[7];
          AdsQuery.Params.ParamByName('ADateofHire').AsDate := StrToDate(s1[8]);
          AdsQuery.Params.ParamByName('ATitle').AsString := s1[9];
          AdsQuery.Params.ParamByName('AStatus').AsString := s1[10];
          AdsQuery.Params.ParamByName('APolicyAssigned').AsString := s1[11];
          AdsQuery.Params.ParamByName('APolicySigned').AsString := s1[12];
          AdsQuery.Params.ParamByName('ATrainingAssigned').AsString := s1[13];
          AdsQuery.Params.ParamByName('ATrainingCompleted').AsString := s1[14];
          AdsQuery.Params.ParamByName('AFormsAssigned').AsString := s1[15];
          AdsQuery.Params.ParamByName('AFormsCompleted').AsString := s1[16];
          AdsQuery.Params.ParamByName('AFormsInNotification').AsString := s1[17];
          AdsQuery.Params.ParamByName('AExclusionary').AsString := s1[18];
          AdsQuery.Params.ParamByName('ADriversLicenseState').AsString := s1[19];
          AdsQuery.Params.ParamByName('ADriversLicenseID').AsString := s1[20];
          AdsQuery.ExecSQL;
          for i := 0 to AdsQuery.ParamCount -1 do
            AdsQuery.Params.Items[i].AsString := '';
          s1.Clear;
        end;
      end;
      loadUsersPassed := true;
    except
      on E: Exception do
      begin
        if length(eMessage) > 6 then
          eMessage := eMessage  + #13#10 +  'TDM.loadUsers failed: ' + E.Message
        else
          eMessage := eMessage + ' TDM.loadUsers failed: ' + E.Message;
        exit;
        exit;
      end;
    end;
  finally
    s1.Free;
    CloseFile(F);
  end;
end;

procedure TDM.SendMail(Subject, Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
//        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
//            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
//            '''' + 'send mail failed' + '''' + ', ' +
//            '''' + 'no sql - line 427' + '''' + ', ' +
//            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;

procedure TDM.updatePtoUsed;
begin
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'execute procedure pto_update_pto_used()';
    AdsQuery.ExecSQL;
    updatePtoUsedPassed := true;
  except
    on E: Exception do
    begin
      if length(eMessage) > 6 then
        eMessage := eMessage  + #13#10 +  'TDM.updatePtoUsed failed: ' + E.Message
      else
        eMessage := eMessage + ' TDM.updatePtoUsed failed: ' + E.Message;
      exit;
    end;
  end;
end;

procedure TDM.updateRequests;
begin
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'execute procedure pto_update_requests()';
    AdsQuery.ExecSQL;
    updateRequestsPassed := true;
  except
    on E: Exception do
    begin
      if length(eMessage) > 6 then
        eMessage := eMessage  + #13#10 +  'TDM.updateRequests failed: ' + E.Message
      else
        eMessage := eMessage + ' TDM.updateRequests failed: ' + E.Message;
      exit;
    end;
  end;
end;

procedure TDM.updateUsers;
begin
  try
    AdsQuery.Close;
    AdsQuery.SQL.Clear;
    AdsQuery.SQL.Text := 'execute procedure pto_update_compli_users()';
    AdsQuery.ExecSQL;
    updateUsersPassed := true;
  except
    on E: Exception do
    begin
      if length(eMessage) > 6 then
        eMessage := eMessage  + #13#10 +  'TDM.updateUsers failed: ' + E.Message
      else
        eMessage := eMessage + ' TDM.updateUsers failed: ' + E.Message;
      exit;
    end;
  end;
end;

end.
