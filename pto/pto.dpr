program pto;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  StrUtils,
  ptoDM in 'ptoDM.pas' {DM: TDataModule};

begin

  try
    try
      DM := TDM.Create(nil);
      DM.ftpCompliFiles;
      if ftpPassed then DM.loadUsers;
      if ftpPassed then DM.loadTimeOff;
      if loadTimeOffPassed then DM.updateRequests;
      if loadUsersPassed then DM.updateUsers;
      DM.updatePtoUsed;
      if not (ftpPassed and loadUsersPassed and loadTimeOFfPassed
        and updateRequestsPassed and updateUsersPassed
        and updatePtoUsedPassed) then DM.SendMail('pto failed: ', eMessage);
    except
      on E: Exception do
      begin
        DM.SendMail('pto failed', E.Message);
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
